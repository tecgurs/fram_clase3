<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/cursos', 'CursosController@index');

Route::get('/cursos/{id}', 'CursosController@detalle')->where('id', '\d+');

Route::get('/grupos', 'GruposController@index');

Route::get('/grupos/nuevo', 'GruposController@nuevo');

Route::get('/grupos/editar/{id}', 'GruposController@editar')->where('id', '\d+');

Route::get('/grupos/borrar/{id}', 'GruposController@borrar')->where('id', '\d+');

Route::get('/grupos/cargo/{folio}', 'GruposController@cargo')->where('folio', '[\da-f]{16}');

Route::get('/test', 'TestController@index');
