<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GruposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupos')->insert([
            'fecha_inicio' => '2019-11-11',
            'cursoId' => DB::table('cursos')->where('code', 'desde0')->first()->id,
            'sedeId' => DB::table('sedes')->where('code', 'monterrey')->first()->id,
            'costo_centavos' => 150000,
        ]);

        DB::table('grupos')->insert([
            'fecha_inicio' => '2019-11-18',
            'cursoId' => DB::table('cursos')->where('code', 'javascript')->first()->id,
            'sedeId' => DB::table('sedes')->where('code', 'monterrey')->first()->id,
            'costo_centavos' => 150000,
        ]);

        DB::table('grupos')->insert([
            'fecha_inicio' => '2020-03-09',
            'cursoId' => DB::table('cursos')->where('code', 'desde0')->first()->id,
            'sedeId' => DB::table('sedes')->where('code', 'monterrey')->first()->id,
            'costo_centavos' => 150000,
        ]);

        DB::table('grupos')->insert([
            'fecha_inicio' => '2019-11-25',
            'cursoId' => DB::table('cursos')->where('code', 'desde0')->first()->id,
            'sedeId' => DB::table('sedes')->where('code', 'cdmx')->first()->id,
            'costo_centavos' => 150000,
        ]);

        DB::table('grupos')->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
