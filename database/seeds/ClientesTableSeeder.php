<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'email' => 'alberto.alvarez@gmail.com',
            'name' => 'Alberto Alvarez',
        ]);

        DB::table('clientes')->insert([
            'email' => 'beto.bardales@gmail.com',
            'name' => 'Roberto Bardales',
        ]);

        DB::table('clientes')->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
