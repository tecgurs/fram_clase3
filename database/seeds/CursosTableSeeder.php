<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cursos')->insert([
            'code' => 'desde0',
            'caption' => 'PHP desde cero',
            'description' => 'Este es un curso para comenzar a programar en PHP',
            'temario_filename' => 'desde_cero.pdf',
        ]);

        DB::table('cursos')->insert([
            'code' => 'frameworks',
            'caption' => 'Frameworks empresariales',
            'description' => 'Aprende Laravel',
            'temario_filename' => 'laravel.pdf',
        ]);

        DB::table('cursos')->insert([
            'code' => 'javascript',
            'caption' => 'Javascript básico',
            'description' => 'Aprende Javascript desde cero',
            'temario_filename' => 'javascript.pdf',
        ]);

        DB::table('cursos')->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
