<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SedesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sedes')->insert([
            'code' => 'monterrey',
            'caption' => 'Campus Monterrey',
        ]);

        DB::table('sedes')->insert([
            'code' => 'cdmx',
            'caption' => 'Campus CDMX',
        ]);

        DB::table('sedes')->update([
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
