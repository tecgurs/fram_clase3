<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('folio', '16')->unique()->nullable();
            $table->unsignedInteger('clienteId')->index();
            $table->unsignedInteger('grupoId')->index();
            $table->integer('importe_centavos');
            $table->integer('iva_centavos');
            $table->string('metodo_pago', '16')->index(); // card | bank | store
            $table->string('estatus', '32');
            $table->boolean('pagado')->default('0');
            $table->timestamp('fecha_pago')->nullable();
            $table->text('charge_object')->nullable();
            $table->string('openpay_id', '100')->nullable()->index();
            $table->string('urlCard', '100')->nullable();
            $table->string('reference', '100')->nullable();

            $table->foreign('clienteId')->references('id')->on('clientes');
            $table->foreign('grupoId')->references('id')->on('grupos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargos');
    }
}
