<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('fecha_inicio');
            $table->unsignedInteger('cursoId')->index();
            $table->unsignedInteger('sedeId')->index();
            $table->integer('costo_centavos');

            $table->foreign('cursoId')->references('id')->on('cursos');
            $table->foreign('sedeId')->references('id')->on('sedes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupos');
    }
}
