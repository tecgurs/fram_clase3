<?php

namespace App\Http\Controllers;

use App\Grupo;
use Illuminate\Http\Request;

class CursosController extends Controller
{
    function index()
    {
        $grupos = Grupo::all();
        return view('cursos.index')->with('grupos', $grupos);
    }

    function detalle(int $id)
    {
        //$collection
        $grupo = Grupo::find($id);
        return view('cursos.detalle')->with('grupo', $grupo);
    }
}
