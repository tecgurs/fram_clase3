<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Tg\Escuela\Cargos\Item as CargosItem;
use App\Tg\Escuela\Clientes\Item as ClientesItem;
use App\Tg\Escuela\Cursos\Item as CursosItem;
use App\Tg\Escuela\Grupos\Item as GruposItem;
use App\Tg\Folios\Item as FoliosItem;
use App\Tg\Remotes\Openpay\Sdk;
use Illuminate\Http\Request;

class TestController extends Controller
{
    function index()
    {
        $clienteItem = ClientesItem::readFromDb(2);
        $grupoItem = GruposItem::readFromDb(2);
        $cargoItem = CargosItem::crearCardCargo($clienteItem, $grupoItem);
        return redirect($cargoItem->getUrlCard());

        //$cargoItem = CargosItem::readFromDb(6);
        //dd($cargoItem);
        //dd($cargoItem->isCharged());

    }
}
