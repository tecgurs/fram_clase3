<?php

namespace App\Http\Controllers;

use App\Sede;
use App\Tg\Escuela\Cargos\Item as CargosItem;
use App\Tg\Escuela\Cursos\Catalog as CursosCatalog;
use App\Tg\Escuela\Grupos\Catalog;
use App\Tg\Escuela\Grupos\Item;
use Illuminate\Http\Request;

class GruposController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    function index()
    {
        $catalog = Catalog::readFromDb();
        return view('grupos.index')->with('grupos', $catalog->getItems());
    }

    function nuevo()
    {
        $sedes = Sede::all();
        $cursosCatalog = CursosCatalog::readFromDb();
        return view('grupos.nuevo')
            ->with('sedes', $sedes)
            ->with('cursos', $cursosCatalog->getItems());
    }

    function editar(int $id)
    {
        try {
            $grupoItem = Item::readFromDb($id);
        } catch (\Exception $e) {
            return redirect('grupos');
        }
        return view('grupos.editar')->with('grupoItem', $grupoItem);
    }

    function borrar(int $id)
    {
        try {
            $grupoItem = Item::readFromDb($id);
        } catch (\Exception $e) {
            return redirect('grupos');
        }
        return view('grupos.borrar')->with('grupoItem', $grupoItem);
    }

    function cargo(string $folio)
    {
        try {
            $cargoItem = CargosItem::findByFolio($folio);
        } catch (\Exception $e) {
            return redirect('grupos');
        }
        if (!$cargoItem->isCharged()) {
            return redirect('grupos');
        }

        return view('grupos.cargo')->with('cargo', $cargoItem);
        //dd($folio);
    }
}
