<?php
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 19/10/19 13:26
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Escuela\Clientes;


use App\Cliente;
use App\Tg\Folios\Item as FoliosItem;
use App\Tg\Remotes\Openpay\Sdk;
use OpenpayCustomer;

class Item
{
    /** @var int */
    private $id;
    /** @var string */
    private $email;
    /** @var string */
    private $name;
    /** @var string */
    private $folio;
    /** @var string */
    private $openpay_id;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    function getOpenpayId(): string
    {
        /*$folio =*/ $this->getFolio();
        if (empty($this->openpay_id)) {
            $openpay = new Sdk();
            $clienteModel = $this->requireModel();
            $clienteOpenpay = $openpay->customersAdd($clienteModel);
            $openpayId = $clienteOpenpay->__get('id');
            $clienteModel->openpay_id = $openpayId;
            $clienteModel->save();
            $this->openpay_id = $openpayId;
        }

        return $this->openpay_id;
    }

    function getOpenpayCustomer(): OpenpayCustomer
    {
        $openpay = new Sdk();
        $openpayCustomer = $openpay->customersGet($this->getOpenpayId());

        return $openpayCustomer;
    }

    static function readFromModel(Cliente $model): Item
    {
        $item = new self();
        $item->id = (int) $model->id;
        $item->email = $model->email;
        $item->name = $model->name;
        $item->folio = (string) $model->folio;
        $item->openpay_id = (string) $model->openpay_id;

        return $item;
    }

    static function readFromDb(int $id): Item
    {
        $model = Cliente::find($id);

        return self::readFromModel($model);
    }

    /**
     * @return string
     */
    private function getFolio(): string
    {
        if (empty($this->folio)) {
            $folio = FoliosItem::nuevo();
            $this->folio = $folio;
            $model = $this->requireModel();
            $model->folio = $folio;
            $model->save();
        }
        return $this->folio;
    }

    private function requireModel(): Cliente
    {
        $model = Cliente::find($this->id);

        return $model;
    }
}
