<?php
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 26/10/19 13:09
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Escuela\Cursos;


use App\Curso;

class Item
{
    /** @var int */
    private $id;
    /** @var string */
    private $code;
    /** @var string */
    private $caption;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    function getCaption(): string
    {
        return $this->caption;
    }

    static function readFromModel(Curso $model): Item
    {
        $item = new self();
        $item->id = (int) $model->id;
        $item->code = $model->code;
        $item->caption = $model->caption;

        return $item;
    }

    static function readFromDb(int $id): Item
    {
        $model = Curso::find($id);

        return self::readFromModel($model);
    }
}
