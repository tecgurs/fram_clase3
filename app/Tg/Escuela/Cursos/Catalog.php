<?php
/**
 * @Filename: Catalog.php
 * @Description:
 * @CreatedAt: 02/11/19 10:42
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Escuela\Cursos;


use App\Curso;

class Catalog
{
    /** @var Item[] */
    private $items = [];

    private function __construct()
    {
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    static function readFromDb(): Catalog
    {
        $catalog = new self();

        $cursos = Curso::all();
        if (count($cursos) == 0) {
            return $catalog;
        }

        foreach ($cursos as $curso) {
            $catalog->items[] = Item::readFromModel($curso);
        }

        return $catalog;
    }
}
