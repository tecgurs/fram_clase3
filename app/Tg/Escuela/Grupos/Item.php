<?php
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 26/10/19 13:21
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Escuela\Grupos;


use App\Grupo;
use App\Tg\Datatypes\Money;
use App\Tg\Escuela\Cursos\Item as CursosItem;

class Item
{
    /** @var int */
    private $id;
    /** @var string */
    private $fecha_inicio;
    /** @var CursosItem */
    private $cursoItem;
    /** @var int */
    private $sedeId;
    /** @var string */
    private $sedeCode;
    /** @var string */
    private $sedeCaption;
    /** @var int */
    private $costo_centavos;
    /** @var Money */
    private $costoMoney;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFechaInicio(): string
    {
        return $this->fecha_inicio;
    }

    function getCursoCaption(): string
    {
        return $this->cursoItem->getCaption();
    }

    /**
     * @return string
     */
    public function getSedeCaption(): string
    {
        return $this->sedeCaption;
    }

    /**
     * @return int
     */
    public function getCostoCentavos(): int
    {
        return $this->costo_centavos;
    }

    function getCostoString(): string
    {
        return $this->costoMoney->getString('$ ', 1, Money::FLAG_IVA_INCLUIDO);
    }

    function generateDescripcion(): string
    {
        return "Curso {$this->cursoItem->getCaption()} en {$this->sedeCaption}";
    }

    static function readFromModel(Grupo $model): Item
    {
        $sedeModel = $model->sede;

        $item = new self();
        $item->id = (int) $model->id;
        $item->fecha_inicio = $model->fecha_inicio;
        $item->cursoItem = CursosItem::readFromDb($model->cursoId);
        $item->sedeId = $sedeModel->id;
        $item->sedeCode = $sedeModel->code;
        $item->sedeCaption = $sedeModel->caption;
        $item->costo_centavos = $model->costo_centavos;
        $item->costoMoney = Money::fromCentavos($model->costo_centavos);

        return $item;
    }

    /**
     * @param int $id
     * @return Item
     * @throws \Exception
     */
    static function readFromDb(int $id): Item
    {
        $model = Grupo::find($id);

        if(!$model) {
            throw new \Exception("No se pudo obtener el model");
        }

        return self::readFromModel($model);
    }
}
