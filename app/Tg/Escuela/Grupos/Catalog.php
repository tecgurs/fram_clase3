<?php
/**
 * @Filename: Catalog.php
 * @Description:
 * @CreatedAt: 02/11/19 10:42
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Escuela\Grupos;


use App\Grupo;

class Catalog
{
    /** @var Item[] */
    private $items = [];

    private function __construct()
    {
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    static function readFromDb(): Catalog
    {
        $catalog = new self();

        $grupos = Grupo::all();
        if (count($grupos) == 0) {
            return $catalog;
        }

        foreach ($grupos as $grupo) {
            $catalog->items[] = Item::readFromModel($grupo);
        }

        return $catalog;
    }
}
