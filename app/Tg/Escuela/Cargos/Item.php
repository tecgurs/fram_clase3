<?php
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 26/10/19 11:17
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Escuela\Cargos;


use App\Cargo;
use App\Tg\Escuela\Clientes\Item as ClientesItem;
use App\Tg\Escuela\Grupos\Item as GruposItem;
use App\Tg\Folios\Item as FoliosItem;
use App\Tg\Remotes\Openpay\Sdk;
use Illuminate\Database\Eloquent\Builder;
use stdClass;

class Item
{
    const FACTOR_IVA = 0.16;
    const METODO_CARD = 'card';
    const METODO_BANK = 'bank_account';
    const METODO_STORE = 'store';
    const ESTATUS_CREADO = 'creado';
    const ESTATUS_ESPERA = 'en-espera-de-pago';
    const ESTATUS_COBRADO = 'cobrado';
    const ESTATUS_ERROR = 'error';

    private static $metodosPago = [
        self::METODO_CARD => 'Tarjeta de crédito',
        self::METODO_BANK => 'Transferencia SPEI',
        self::METODO_STORE => 'Pago en tiendas',
    ];

    /** @var int */
    private $id;
    /** @var string */
    private $folio;
    /** @var ClientesItem */
    private $clienteItem;
    /** @var int */
    private $importe_centavos;
    /** @var int */
    private $iva_centavos;
    /** @var GruposItem */
    private $grupoItem;
    /** @var string */
    private $metodo_pago;
    /** @var string */
    private $estatus;
    /** @var bool */
    private $pagado;
    /** @var stdClass */
    private $chargeObject;
    /** @var string */
    private $openpayId;
    /** @var string */
    private $urlCard;
    /** @var string */
    private $reference;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFolio(): string
    {
        return $this->folio;
    }

    /**
     * @return ClientesItem
     */
    function getClienteItem(): ClientesItem
    {
        return $this->clienteItem;
    }

    function generateGrupoDescripcion(): string
    {
        return $this->grupoItem->generateDescripcion();
    }

    /**
     * @return string
     */
    function getMetodoPago(): string
    {
        return $this->metodo_pago;
    }

    function getCaptionMetodoPago(): string
    {
        return self::$metodosPago[$this->metodo_pago];
    }

    /**
     * @return stdClass
     */
    function getChargeObject(): stdClass
    {
        if (empty($this->chargeObject)) {
            return (object) [];
        }
        return $this->chargeObject;
    }

    /**
     * @return string
     */
    public function getOpenpayId(): string
    {
        return $this->openpayId;
    }

    /**
     * @return string
     */
    public function getUrlCard(): string
    {
        if ($this->metodo_pago == self::METODO_CARD && $this->estatus == self::ESTATUS_ESPERA) {
            return $this->urlCard;
        }

        return '';
    }

    function getTotal(): float
    {
        return ($this->importe_centavos + $this->iva_centavos) / 100;
    }

    function getUrlRedirectCard(): string
    {
        return url("grupos/cargo/{$this->folio}");
    }

    function getAuthorization(): string
    {
        $chargeObject = $this->getChargeObject();
        if (property_exists($chargeObject, 'authorization')) {
            return $chargeObject->authorization;
        }

        return 'N/A';
    }

    function hasCardDetails(): bool
    {
        return !empty($this->getCardDetails());
    }

    function getCardDetails(): string
    {
        if ($this->metodo_pago != self::METODO_CARD) {
            return '';
        }

        $chargeObject = $this->getChargeObject();
        if (!(
            property_exists($chargeObject, 'bank_name') &&
            property_exists($chargeObject, 'card_number')
        )) {
            return '';
        }

        $bankName = $chargeObject->bank_name;
        $terminacionCard = substr($chargeObject->card_number, -4);

        return "Tarjeta de  {$bankName} con terminación {$terminacionCard}";
    }

    function isCharged(): bool
    {
        if ($this->pagado) {
            return true;
        }
        if ($this->estatus != self::ESTATUS_ESPERA) {
            return false;
        }

        $sdk = new Sdk();
        $cargo = $sdk->chargesGet($this);
        if ($cargo->__get('status') != 'completed') {
            return false;
        }

        $charge_array = [
            'authorization' => $cargo->__get('authorization'),
            'creation_date' => $cargo->__get('creation_date'),
            'status' => $cargo->__get('status'),
        ];

        if ($this->metodo_pago == self::METODO_CARD) {
            $openpayCard = $cargo->__get('card');
            $charge_array = array_merge($charge_array, [
                'bank_name' => $openpayCard->__get('bank_name'),
                'card_number' => $openpayCard->__get('card_number'),
                'holder_name' => $openpayCard->__get('holder_name'),
            ]);
        }

        $model = $this->requireModel();
        $model->pagado = 1;
        $model->estatus = self::ESTATUS_COBRADO;
        $model->fecha_pago = date('Y-m-d H:i:s');
        $model->charge_object = json_encode((object) $charge_array);
        $model->save();

        return true;
    }

    function generateUrlPdfBank(): string
    {
        if ($this->metodo_pago == self::METODO_BANK && $this->estatus == self::ESTATUS_ESPERA) {
            $openpayIdComercio = Sdk::OPENPAY_ID_COMERCIO;
            return "https://sandbox-dashboard.openpay.mx/spei-pdf/{$openpayIdComercio}/{$this->openpayId}";
        }

        return '';
    }

    function generateUrlPdfStore(): string
    {
        if ($this->metodo_pago == self::METODO_STORE && $this->estatus == self::ESTATUS_ESPERA) {
            $openpayIdComercio = Sdk::OPENPAY_ID_COMERCIO;
            return "https://sandbox-dashboard.openpay.mx/paynet-pdf/{$openpayIdComercio}/{$this->reference}";
        }

        return '';
    }

    function saveOpenpayId(string $openpayId): Item
    {
        $model = $this->requireModel();
        $model->openpay_id = $openpayId;
        $model->estatus = self::ESTATUS_ESPERA;
        $model->save();

        return self::readFromDb($this->id);
    }

    function saveUrlCard(string $openpayId, string $urlCard): Item
    {
        $model = $this->requireModel();
        $model->openpay_id = $openpayId;
        $model->urlCard = $urlCard;
        $model->estatus = self::ESTATUS_ESPERA;
        $model->save();

        return self::readFromDb($this->id);
    }

    function saveStoreReference(string $openpayId, string $reference): Item
    {
        $model = $this->requireModel();
        $model->openpay_id = $openpayId;
        $model->reference = $reference;
        $model->estatus = self::ESTATUS_ESPERA;
        $model->save();

        return self::readFromDb($this->id);
    }

    static function crearCardCargo(ClientesItem $clienteItem, GruposItem $gruposItem): Item
    {
        $model = new Cargo();
        $model->folio = FoliosItem::nuevo('nuevo-cargo-tarjeta');
        $model->clienteId = $clienteItem->getId();
        $model->grupoId = $gruposItem->getId();
        $model->importe_centavos = $gruposItem->getCostoCentavos();
        $model->iva_centavos = round($gruposItem->getCostoCentavos() * self::FACTOR_IVA);
        $model->metodo_pago = self::METODO_CARD;
        $model->estatus = self::ESTATUS_CREADO;
        $model->save();

        $item = self::readFromDb($model->id);
        $sdk = new Sdk();
        $cargo = $sdk->chargesCreateCard($item);

        $item = $item->saveUrlCard($cargo->__get('id'), $cargo->__get('payment_method')->url);

        return $item;
    }

    static function crearBankCargo(ClientesItem $clienteItem, GruposItem $gruposItem): Item
    {
        $model = new Cargo();
        $model->folio = FoliosItem::nuevo('nuevo-cargo-banco');
        $model->clienteId = $clienteItem->getId();
        $model->grupoId = $gruposItem->getId();
        $model->importe_centavos = $gruposItem->getCostoCentavos();
        $model->iva_centavos = round($gruposItem->getCostoCentavos() * self::FACTOR_IVA);
        $model->metodo_pago = self::METODO_BANK;
        $model->estatus = self::ESTATUS_CREADO;
        $model->save();

        $item = self::readFromDb($model->id);
        $sdk = new Sdk();
        $cargo = $sdk->chargesCreateBank($item);

        $item = $item->saveOpenpayId($cargo->__get('id'));

        return $item;
    }

    static function crearStoreCargo(ClientesItem $clienteItem, GruposItem $gruposItem): Item
    {
        $model = new Cargo();
        $model->folio = FoliosItem::nuevo('nuevo-cargo-tienda');
        $model->clienteId = $clienteItem->getId();
        $model->grupoId = $gruposItem->getId();
        $model->importe_centavos = $gruposItem->getCostoCentavos();
        $model->iva_centavos = round($gruposItem->getCostoCentavos() * self::FACTOR_IVA);
        $model->metodo_pago = self::METODO_STORE;
        $model->estatus = self::ESTATUS_CREADO;
        $model->save();

        $item = self::readFromDb($model->id);
        $sdk = new Sdk();
        $cargo = $sdk->chargesCreateStore($item);
        //dd($cargo->__get('payment_method')->reference);

        $item = $item->saveStoreReference($cargo->__get('id'), $cargo->__get('payment_method')->reference);

        return $item;
    }

    static function readFromModel(Cargo $model): Item
    {
        $item = new self();
        $item->id = (int) $model->id;
        $item->folio = $model->folio;
        $item->clienteItem = ClientesItem::readFromDb($model->clienteId);
        $item->importe_centavos = (int) $model->importe_centavos;
        $item->iva_centavos = (int) $model->iva_centavos;
        $item->grupoItem = GruposItem::readFromModel($model->grupo);
        $item->metodo_pago = $model->metodo_pago;
        $item->estatus = $model->estatus;
        $item->pagado = (bool) $model->pagado;
        if (!empty($model->charge_object)) {
            $item->chargeObject = json_decode($model->charge_object);
        }
        $item->openpayId = (string) $model->openpay_id;
        $item->urlCard = (string) $model->urlCard;
        $item->reference = (string) $model->reference;

        return $item;
    }

    static function readFromDb(int $id): Item
    {
        $model = Cargo::find($id);

        return self::readFromModel($model);
    }

    /**
     * @param string $folio
     * @return Item
     * @throws \Exception
     */
    static function findByFolio(string $folio): Item
    {
        /** @var Builder $builder */
        $builder = Cargo::where(['folio' => $folio]);
        /** @var Cargo $model */
        $model = $builder->first();

        if (!$model) {
            throw new \Exception("No existe el cargo {$folio}");
        }

        return self::readFromModel($model);
    }

    private function requireModel(): Cargo
    {
        $model = Cargo::find($this->id);

        return $model;
    }
}
