<?php
/**
 * @Filename: Sdk.php
 * @Description:
 * @CreatedAt: 19/10/19 11:02
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Remotes\Openpay;


use App\Cliente;
use App\Tg\Escuela\Cargos\Item as CargosItem;
use Openpay;
use OpenpayApi;
use OpenpayCharge;
use OpenpayChargeList;
use OpenpayCustomer;
use OpenpayCustomerList;

class Sdk
{
    const OPENPAY_ID_COMERCIO = 'm5pmqhxqk6dcz0knz52g';

    private $config = [
        'id' => self::OPENPAY_ID_COMERCIO,
        'private_key' => 'sk_96cf8249809140188292807e118b818b',
        'public_key' => 'pk_d0a0bc38dc6f4114bc31dd16c3506782',
    ];

    private $instancia;

    function __construct()
    {
        Openpay::setProductionMode(false);
        $instancia = $this->requireInstancia();
    }

    function chargesGet(CargosItem $cargoItem): OpenpayCharge
    {
        $openpayCustomer = $cargoItem->getClienteItem()->getOpenpayCustomer();
        /** @var OpenpayChargeList $charges */
        /** @noinspection PhpUndefinedFieldInspection */
        $charges = $openpayCustomer->charges;
        $charge = $charges->get($cargoItem->getOpenpayId());

        return $charge;
    }

    function chargesCreateCard(CargosItem $cargoItem): OpenpayCharge
    {
        $openpayCustomer = $cargoItem->getClienteItem()->getOpenpayCustomer();
        /** @var OpenpayChargeList $charges */
        /** @noinspection PhpUndefinedFieldInspection */
        $charges = $openpayCustomer->charges;
        $charge = $charges->create([
            'method' => $cargoItem->getMetodoPago(),
            'amount' => $cargoItem->getTotal(),
            'description' => $cargoItem->generateGrupoDescripcion(),
            'send_email' => false,
            'confirm' => false,
            'redirect_url' => $cargoItem->getUrlRedirectCard(),
        ]);

        return $charge;
    }

    function chargesCreateBank(CargosItem $cargoItem): OpenpayCharge
    {
        $openpayCustomer = $cargoItem->getClienteItem()->getOpenpayCustomer();
        /** @var OpenpayChargeList $charges */
        /** @noinspection PhpUndefinedFieldInspection */
        $charges = $openpayCustomer->charges;
        $charge = $charges->create([
            'method' => $cargoItem->getMetodoPago(),
            'amount' => $cargoItem->getTotal(),
            'description' => $cargoItem->generateGrupoDescripcion(),
            'order_id' => $cargoItem->getFolio(),
        ]);

        return $charge;
    }

    function chargesCreateStore(CargosItem $cargoItem): OpenpayCharge
    {
        $openpayCustomer = $cargoItem->getClienteItem()->getOpenpayCustomer();
        /** @var OpenpayChargeList $charges */
        /** @noinspection PhpUndefinedFieldInspection */
        $charges = $openpayCustomer->charges;
        $charge = $charges->create([
            'method' => $cargoItem->getMetodoPago(),
            'amount' => $cargoItem->getTotal(),
            'description' => $cargoItem->generateGrupoDescripcion(),
            'order_id' => $cargoItem->getFolio(),
        ]);

        return $charge;
    }

    function customersAdd(Cliente $clienteModel): OpenpayCustomer
    {
        $instancia = $this->requireInstancia();
        /** @var OpenpayCustomerList $customers */
        /** @noinspection PhpUndefinedFieldInspection */
        $customers = $instancia->customers;
        $customer = $customers->add([
            'external_id' => $clienteModel->folio,
            'name' => $clienteModel->name,
            'email' => $clienteModel->email,
            'requires_account' => false,
        ]);

        return $customer;
    }

    function customersGet(string $openpayId): OpenpayCustomer
    {
        $instancia = $this->requireInstancia();
        /** @var OpenpayCustomerList $customers */
        /** @noinspection PhpUndefinedFieldInspection */
        $customers = $instancia->customers;
        $customer = $customers->get($openpayId);

        return $customer;
    }

    private function requireInstancia(): OpenpayApi
    {
        if (empty($this->instancia)) {
            $this->instancia = Openpay::getInstance($this->config['id'], $this->config['private_key']);
        }

        return $this->instancia;
    }
}
