<?php
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 19/10/19 9:44
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Folios;


use App\Folio;
//use Illuminate\Support\Facades\DB;

class Item
{
    private function __construct()
    {
    }

    static function nuevo(string $proposito = ''): string
    {
        $item = new self();

        return $item->generateUniqueHex($proposito);
    }

    private function generateUniqueHex(string $proposito = ''): string
    {
        $folio = $this->generateValidHex();

        $folioModel = new Folio();
        $folioModel->folio = $folio;
        $folioModel->proposito = $proposito;
        try {
            /*$response =*/ $folioModel->save();
            return $folio;
        } catch (\Exception $e) {
            return $this->generateUniqueHex($proposito);
        }
    }

    private function generateValidHex(): string
    {
        $hex = '';

        while (empty($hex)) {
            $nominado = $this->generateRandomHex();

            if (preg_match("#^[a-f]#", $nominado)) {
                $hex = $nominado;
            }
        }

        return $hex;
    }

    private function generateRandomHex(): string
    {
        $salida = '';
        for ($i = 0; $i < 4; $i++) {
            $salida .= substr(uniqid(), 9, 4);
        }
        return $salida;
    }
}
