<?php
/**
 * @Filename: Money.php
 * @Description:
 * @CreatedAt: 29/10/19 23:47
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Tg\Datatypes;


class Money
{
    const FACTOR_IVA = 1.16;
    const FLAG_SOLO_IMPORTE = 1;
    const FLAG_SOLO_IVA = 2;
    const FLAG_IVA_INCLUIDO = 3;

    /** @var int En centavos sin IVA */
    private $centavos;

    private function __construct(int $centavos)
    {
        $this->centavos = $centavos;
    }

    public function getString(string $prefix = '', int $factor = 1, int $flag = self::FLAG_SOLO_IMPORTE): string
    {
        return $prefix . number_format($this->getFloat($factor, $flag), 2);
    }

    function getFloat(int $factor = 1, int $flag = self::FLAG_SOLO_IMPORTE): float
    {
        return $this->getCentavos($factor, $flag) / 100;
    }

    /**
     * @param int $factor
     * @param int $flag
     * @return int
     */
    function getCentavos(int $factor = 1, int $flag = self::FLAG_SOLO_IMPORTE): int
    {
        $importeCentavos = $this->centavos * $factor;
        if ($flag == self::FLAG_SOLO_IMPORTE) {
            return $importeCentavos;
        }

        $centavosIva = $this->generateCentavosIva($factor);
        if ($flag == self::FLAG_SOLO_IVA) {
            return $centavosIva;
        }

        if ($flag == self::FLAG_IVA_INCLUIDO) {
            return $importeCentavos + $centavosIva;
        }

        return $importeCentavos;
    }

    function incrementCentavos(int $centavos): Money
    {
        $this->centavos += $centavos;

        return $this;
    }

    static function fromCentavos(int $centavos): Money
    {
        return new self($centavos);
    }

    static function fromFloat(float $monto): Money
    {
        return self::fromCentavos(round(100 * $monto));
    }

    static function fromString(string $monto): Money
    {
        return self::fromFloat((float) $monto);
    }

    static function zero(): Money
    {
        return self::fromCentavos(0);
    }

    private function generateCentavosIva(int $factor = 1): int
    {
        return round($this->centavos * $factor * (self::FACTOR_IVA - 1));
    }
}
