<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $caption
 */
class Sede extends Model
{
    function grupos()
    {
        return $this->hasMany(Grupo::class, 'sedeId');
    }
}
