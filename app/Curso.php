<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $caption
 * @method static find(int $id)
 */
class Curso extends Model
{
    function grupos()
    {
        return $this->hasMany(Grupo::class, 'cursoId');
    }
}
