<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $folio
 * @property int $clienteId
 * @property int $grupoId
 * @property Grupo $grupo
 * @property int $importe_centavos
 * @property int $iva_centavos
 * @property string $metodo_pago
 * @property string $estatus
 * @property boolean $pagado
 * @property string $fecha_pago
 * @property string $charge_object
 * @property string $openpay_id
 * @property string $urlCard
 * @property string $reference
 * @method static find(int $id)
 * @method static where(array $array)
 */
class Cargo extends Model
{
    function grupo()
    {
        return $this->belongsTo(Grupo::class, 'grupoId');
    }
}
