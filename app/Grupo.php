<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $fecha_inicio
 * @property int $cursoId
 * @property int $sedeId
 * @property Sede $sede
 * @property int $costo_centavos
 * @method static find(int $id)
 */
class Grupo extends Model
{
    function curso()
    {
        return $this->belongsTo(Curso::class, 'cursoId');
    }

    function sede()
    {
        return $this->belongsTo(Sede::class, 'sedeId');
    }

    function fecha(): string
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $datetime = new DateTime($this->fecha_inicio);

        $dia = $datetime->format('j');
        $anio = $datetime->format('Y');
        $meses = ['',
            'Ene', 'Feb', 'Mar',
            'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep',
            'Oct', 'Nov', 'Dic'
        ];
        $mes = $meses[$datetime->format('n')];

        return "{$dia} de {$mes} de {$anio}";
    }
}
