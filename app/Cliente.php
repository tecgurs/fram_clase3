<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $folio
 * @property string $openpay_id
 * @method static find(int $int)
 */
class Cliente extends Model
{
    //
}
