@extends('layouts.tb4app')

@section('content')

  <div class="container">
    <ul class="nav justify-content-end mb-2">
      <li class="nav-item">
        <a href="{{ url()->previous() }}" class="nav-link">Regresar</a>
      </li>
    </ul>
  </div>

  <h5>Crear nuevo grupo</h5>

  <div class="row">
    <div class="col-12 col-md-6 offset-md-3">
      <form>
        <div class="form-row">

          <fieldset class="form-group col-12 col-lg-6">
            <label for="sede">Sede</label>
            <select id="sede" name="sede" class="form-control">
              <option value="0">...Selecciona la sede...</option>
              @foreach($sedes as $sede)
                <option value="{{ $sede->id }}">{{ $sede->caption }}</option>
              @endforeach
            </select>
          </fieldset>

          <fieldset class="form-group col-12 col-lg-6">
            <label for="curso">Curso</label>
            <select id="curso" name="curso" class="form-control">
              <option value="0">...Selecciona el curso...</option>
              @foreach($cursos as $curso)
                <option value="{{ $curso->getId() }}">{{ $curso->getCaption() }}</option>
              @endforeach
            </select>
          </fieldset>

          <fieldset class="form-group col-12 col-lg-6">
            <label for="fecha_inicio">Fecha de inicio [aaaa-mm-dd]</label>
            <input type="text" id="fecha_inicio" name="fecha_inicio" class="form-control">
            <div class="invalid-feedback">Este campo es requerido</div>
          </fieldset>

          <fieldset class="form-group col-12 col-lg-6">
            <label for="costo">Costo</label>
            <input type="text" id="costo" name="costo" class="form-control">
            <div class="invalid-feedback">Este campo es requerido</div>
          </fieldset>

          <fieldset class="form-group col-12">
            <button class="btn btn-success btn-block" type="button" disabled>Crear grupo</button>
          </fieldset>

        </div>
      </form>
    </div>
  </div>

@endsection
