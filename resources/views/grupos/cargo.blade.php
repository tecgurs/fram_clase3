@extends('layouts.tb4app')

@section('content')

  <table class="table">
    <tr><th>Descripcion</th><td>{{ $cargo->generateGrupoDescripcion() }}</td></tr>
    <tr><th>Estatus de cobro</th><td>Cobrado</td></tr>
    <tr><th>Método de pago</th><td>{{ $cargo->getCaptionMetodoPago() }}</td></tr>
    <tr><th>Autorización</th><td>{{ $cargo->getAuthorization() }}</td></tr>
    @if($cargo->hasCardDetails())
      <tr><th>Tarjeta</th><td>{{ $cargo->getCardDetails() }}</td></tr>
    @endif

  </table>


@endsection
