@extends('layouts.tb4app')

@section('content')

  <div class="container">
    <ul class="nav justify-content-end mb-2">
      <li class="nav-item">
        <a href="{{ url()->previous() }}" class="nav-link">Regresar</a>
      </li>
    </ul>
  </div>

  <h5>¿Estás seguro de que deseas borrar el grupo: {{ $grupoItem->generateDescripcion() }}?</h5>

@endsection
