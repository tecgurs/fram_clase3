@extends('layouts.tb4app')

@section('content')

  <div class="container">
    <ul class="nav justify-content-end mb-2">
      <li class="nav-item">
        <a href="{{ url("grupos/nuevo") }}" class="nav-link">Nuevo grupo</a>
      </li>
    </ul>
  </div>

@forelse($grupos as $grupo)
  @if($loop->first)
    <table class="table">
      <tr><th>Fecha</th><th>Curso</th><th>Sede</th><th>Costo</th><th>Acciones</th></tr>
  @endif
      <tr>
        <td>{{ $grupo->getFechaInicio() }}</td>
        <td>{{ $grupo->getCursoCaption() }}</td>
        <td>{{ $grupo->getSedeCaption() }}</td>
        <td>{{ $grupo->getCostoString() }}</td>
        <td>
          <a href="{{ url("grupos/editar/{$grupo->getId()}") }}">Editar</a> |
          <a href="{{ url("grupos/borrar/{$grupo->getId()}") }}">Borrar</a>
        </td>
      </tr>
  @if($loop->last)
     </table>
  @endif
@empty

<h5>No hay grupos</h5>

@endforelse

@endsection
