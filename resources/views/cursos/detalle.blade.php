@extends('layouts.cursos')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $grupo->curso->caption or 'No hay curso' }}</div>

                    @empty($grupo)
                        No está definido
                    @else
                        <div class="panel-body">
                            <table class="table">
                                <tr><th>Fecha de inicio</th><td>{{ $grupo->fecha() }}</td></tr>
                                <tr><th>Ciudad</th><td>{{ $grupo->sede->caption }}</td></tr>
                                <tr><th>Descripción</th><td>{{ $grupo->curso->description }}</td></tr>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
