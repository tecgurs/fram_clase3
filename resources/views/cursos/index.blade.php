@extends('layouts.cursos')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Nuestros Cursos</div>

                    <div class="panel-body">
                        @empty($grupos)
                            No hay cursos disponibles
                        @else
                            @foreach($grupos as $grupo)
                                @if($loop->first)
                                    <table class="table">
                                        <tr><th>Curso</th><th>Ciudad</th><th>Inicio</th><th>Detalles</th></tr>
                                @endif

                                <tr>
                                    <th>{{ $grupo->curso->caption }}</th>
                                    <th>{{ $grupo->sede->caption }}</th>
                                    <th>{{ $grupo->fecha() }}</th>
                                    <th><a href="{{ url('/cursos/' . $grupo->id) }}">Ir</a></th>
                                </tr>

                                @if($loop->last)
                                    </table>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
